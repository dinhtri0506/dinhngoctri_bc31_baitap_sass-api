export let LAYOUT_CONTROLLER = {
  classList_toggle: (id, className) => {
    document.getElementById(id).classList.toggle(className);
  },
  render_id_input_form: () => {
    let contentHTML = ` <div class="modal-content">
    <div class="modal-header">
      <h5 class="modal-title" id="exampleModalLabel">Edit dish info</h5>
      <button
        type="button"
        class="close"
        data-dismiss="modal"
        aria-label="Close"
      >
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">
      <div>
        <div class="form-group">
          <label for="">ID or name of the dish</label>
          <input
            type="text"
            class="form-control"
            name=""
            id="dish-edited"
            aria-describedby="helpId"
            placeholder="Input id or name of the dish"
          />
          <span id="id-edit-notice"></span>
        </div>
      </div>     
    </div>
    <div class="modal-footer">
      <button
        type="button"
        class="btn btn-warning"
        onclick="find_dish()"
      >
        Next
      </button>     
    </div>
  </div>`;
    document.getElementById("edit-form").innerHTML = contentHTML;
  },
  render_edit_form: () => {
    let editForm = document.getElementById("edit-form");
    editForm.innerHTML = "";
    let contentHTML = ` <div class="modal-header">
    <h5 class="modal-title" id="exampleModalLabel">Edit dish info</h5>
    <button
      type="button"
      class="close"
      data-dismiss="modal"
      aria-label="Close"
    >
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
  <div class="modal-body">  
    <div>
      <div class="form-group">
        <label for="">ID</label>
        <input
          type="text"
          class="form-control"
          name=""
          id="id-edit-form"
          aria-describedby="helpId"
          placeholder=""
          disabled
        />
      </div>
      <div class="form-group">
        <label for="">Name</label>
        <input
          type="text"
          class="form-control"
          name=""
          id="name-edit-form"
          aria-describedby="helpId"
          placeholder="Input name of dish"
          disabled
        />
      </div>
      <div class="form-group">
        <label for="">Type</label>
        <input
          type="text"
          class="form-control"
          name=""
          id="type-edit-form"
          aria-describedby="helpId"
          placeholder="Input type of dish"
        />
        <span id="type-edit-notice"></span>
      </div>
      <div class="form-group">
        <label for="">Price</label>
        <input
          type="number"
          class="form-control"
          name=""
          id="price-edit-form"
          aria-describedby="helpId"
          placeholder="Input price of dish"
        />
      </div>
      <div class="form-group">
        <label for="">Dish status</label>
        <select class="form-control" name="" id="status-edit-form">
          <option>Available</option>
          <option>Ran out</option>
        </select>
      </div>
      <div class="form-group">
        <label for="">Description</label>
        <textarea
          class="form-control"
          name=""
          id="description-edit-form"
          rows="3"
        ></textarea>
      </div>
    </div>
  </div>
  <div class="modal-footer">   
    <button type="button" class="btn btn-warning" onclick="update_dish_info()">
      Update
    </button>
    <button type="button" class="btn btn-secondary" onclick="reset_edit_form()">
      Reset
    </button>
  </div>`;
    editForm.innerHTML = contentHTML;
  },
};
