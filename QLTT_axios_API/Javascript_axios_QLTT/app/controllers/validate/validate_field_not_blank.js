function validate_field_not_blank(field, noticeEl, fieldName) {
  if (field != "") {
    noticeEl.innerHTML = "";
    return true;
  }
  noticeEl.innerHTML = `${fieldName} không được để trống!`;
  return false;
}
