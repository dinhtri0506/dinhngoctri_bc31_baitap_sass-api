let find_index = (array, propertyName, value) => {
  let index = array.findIndex((item) => {
    return item[propertyName] == value;
  });
  return index;
};
