import { DOM_MODELS } from "../models/menu_models.js";

export let DISH_CONTROLLER = {
  dom_all_dish_input: (...array) => {
    let inputArray = [];
    for (let i = 0; i < array.length; i++) {
      let inputId = array[i];
      let inputEl = document.querySelector(inputId);
      inputArray.push(inputEl);
    }
    return inputArray;
  },
  add_value_for_input: (inputArray, ...value) => {
    for (let i = 0; i < inputArray.length; i++) {
      inputArray[i].value = value[i];
    }
  },
  render_menu: (array, tableID) => {
    let contentHTML = "";
    for (let i = 0; i < array.length; i++) {
      let item = array[i];
      let contentRendered = `<tr>
      <th class="text-center" scope="row">${item.id}</th>
      <td>${item.name}</td>
      <td class="text-center">${item.type}</td>
      <td class="text-center">${item.price}</td>
      <td class="text-center">${item.status}</td>
      <td class="text-center">
      <button class="edit-btn btn btn-warning" 
      data-toggle="modal"
      data-target="#editDishInfoModal"
      onclick="put_data_back_to_form(${item.id})">
      <i class="fa fa-sync-alt"></i>
      </button>
      <button class="delete-btn btn btn-danger" 
      onclick="delete_dish(${item.id})">
      <i class="fa fa-trash-alt"></i>
      </button>
      </td>
    </tr>`;
      contentHTML += contentRendered;
    }
    document.getElementById(tableID).innerHTML = contentHTML;
  },
  find_index: (array, property, value) => {
    let index = array.findIndex((item) => {
      let itemProperty = item[property].toLowerCase();
      return itemProperty == value;
    });
    return index;
  },
  add_value_for_edit_input: (dish) => {
    DISH_CONTROLLER.add_value_for_input(
      DOM_MODELS.edit_el_array(),
      dish.id,
      dish.name,
      dish.type,
      dish.price,
      dish.status,
      dish.description
    );
  },
};
