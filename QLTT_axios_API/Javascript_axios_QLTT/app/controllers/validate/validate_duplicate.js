function validate_duplicate(condition, noticeEl, noticeContent) {
  if (condition) {
    noticeEl.innerHTML = "";
    return true;
  }
  noticeEl.innerHTML = noticeContent;
  return false;
}
