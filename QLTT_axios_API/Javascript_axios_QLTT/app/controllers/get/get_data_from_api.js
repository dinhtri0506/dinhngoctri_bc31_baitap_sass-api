let get_data_from_api = () => {
  loading_screen_on();
  axios({
    url: "https://62b07878e460b79df0469b79.mockapi.io/QLTT_user",
    method: "GET",
  })
    .then(function (result) {
      userArray = result.data;
      render_user_info();
      loading_screen_off();
    })
    .catch(function (error) {
      console.log(error);
      loading_screen_off();
    });
};
window.onload = () => {
  get_data_from_api();
};
