function add() {
  let accountValid = add_account_validate();
  let otherValid = validate();
  if (accountValid && otherValid) {
    add_data_to_api();
  }
}

function update() {
  let accountValid = update_account_validate();
  let otherValid = validate();
  if (accountValid && otherValid) {
    update_user_info();
  }
}
