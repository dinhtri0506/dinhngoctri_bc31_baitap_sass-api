function validate_format(regEX, inputValue, noticeEl, noticeContent) {
  if (regEX.test(inputValue)) {
    noticeEl.innerHTML = "";
    return true;
  }
  noticeEl.innerHTML = noticeContent;
  return false;
}
