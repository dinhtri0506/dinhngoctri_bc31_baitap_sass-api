function delete_user(id) {
  loading_screen_on();
  axios({
    url: "https://62b07878e460b79df0469b79.mockapi.io/QLTT_user/" + id,
    method: "DELETE",
  })
    .then(function (result) {
      get_data_from_api();
      loading_screen_off();
    })
    .catch(function (error) {
      console.log("error: ", error);
      loading_screen_off();
    });
}
