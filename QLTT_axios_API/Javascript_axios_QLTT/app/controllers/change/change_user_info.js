function change_user_info(id) {
  axios({
    url: "https://62b07878e460b79df0469b79.mockapi.io/QLTT_user/" + id,
    method: "GET",
  })
    .then(function (result) {
      reset();
      let user = result.data;
      // let domArray = dom(
      //   "#TaiKhoan",
      //   "#HoTen",
      //   "#MatKhau",
      //   "#Email",
      //   "#loaiNguoiDung",
      //   "#loaiNgonNgu",
      //   "#MoTa",
      //   "#HinhAnh"
      // );
      domArray[0].value = user.taiKhoan;
      domArray[1].value = user.hoTen;
      domArray[2].value = user.matKhau;
      domArray[3].value = user.email;
      domArray[4].value = user.loaiND;
      domArray[5].value = user.ngonNgu;
      domArray[6].value = user.moTa;
      domArray[7].value = user.hinhAnh;
    })
    .catch(function (error) {
      console.log("error: ", error);
    });
}
