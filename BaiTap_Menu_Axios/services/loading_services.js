export let LOADING_SERVICES = {
  turn_on_loading_screen: () => {
    document.getElementById("loading-screen").style.display = "flex";
  },
  turn_off_loading_screen: () => {
    document.getElementById("loading-screen").style.display = "none";
  },
};
