const BASE_URL = "https://62b07878e460b79df0469b79.mockapi.io/Menu";

export let MENU_SERVICES = {
  add_data_to_API: (dish) => {
    return axios({
      url: BASE_URL,
      method: "POST",
      data: dish,
    });
  },
  get_data_from_API: () => {
    return axios({
      url: BASE_URL,
      method: "GET",
    });
  },
  delete_data: (id) => {
    return axios({
      url: BASE_URL + "/" + id,
      method: "DELETE",
    });
  },
  update_data: (id, dish) => {
    return axios({
      url: BASE_URL + "/" + id,
      method: "PUT",
      data: dish,
    });
  },
};
