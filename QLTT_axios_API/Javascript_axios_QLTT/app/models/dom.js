function dom(...rest) {
  let domArray = [];
  for (let i = 0; i < rest.length; i++) {
    let id = rest[i];
    let dom = document.querySelector(id);
    domArray.push(dom);
  }
  return domArray;
}

let domArray = dom(
  "#TaiKhoan",
  "#HoTen",
  "#MatKhau",
  "#Email",
  "#loaiNguoiDung",
  "#loaiNgonNgu",
  "#MoTa",
  "#HinhAnh"
);

let noticeDomArray = dom(
  "#account-notice",
  "#name-notice",
  "#password-notice",
  "#email-notice",
  "#user-type-notice",
  "#language-notice",
  "#describe-notice",
  "#image-notice"
);
