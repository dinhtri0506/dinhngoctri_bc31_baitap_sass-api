import { LAYOUT_CONTROLLER } from "./controller/layout_controller.js";
import { DISH, DISH_EDITED, DOM_MODELS } from "./models/menu_models.js";
import { DISH_CONTROLLER } from "./controller/dish_controller.js";
import { MENU_SERVICES } from "./services/menu_services.js";
import { LOADING_SERVICES } from "./services/loading_services.js";
import { VALIDATE_CONTROLLER } from "./controller/validate_controller.js";

let MENU = [];

let swap_action = (id, className) => {
  LAYOUT_CONTROLLER.classList_toggle(id, className);
};
window.swap_action = swap_action;

let render_id_input_form = () => {
  LAYOUT_CONTROLLER.render_id_input_form();
};
window.render_id_input_form = render_id_input_form;

let render_edit_form = () => {
  LAYOUT_CONTROLLER.render_edit_form();
};
window.render_edit_form = render_edit_form;

/**
 *  SERVICES
 */

// ADD NEW DISH
let add_new_dish = () => {
  let valid = VALIDATE_CONTROLLER.add_new_null_check();
  if (valid) {
    LOADING_SERVICES.turn_on_loading_screen();
    let dish = DISH();
    let name = dish.name.toLowerCase();
    let index = DISH_CONTROLLER.find_index(MENU, "name", name);
    if (index >= 0) {
      let duplicateDish = MENU[index];
      let noticeContent = `The name of this dish already in your menu (id: ${duplicateDish.id})\nDo you want to update information for it?`;
      if (confirm(noticeContent) == true) {
        LOADING_SERVICES.turn_on_loading_screen();
        MENU_SERVICES.update_data(duplicateDish.id, dish)
          .then((success) => {
            render_menu();
            LOADING_SERVICES.turn_off_loading_screen();
          })
          .catch((error) => {
            console.log(error);
            LOADING_SERVICES.turn_off_loading_screen();
          });
        return;
      }
      LOADING_SERVICES.turn_off_loading_screen();
      return;
    }
    MENU_SERVICES.add_data_to_API(dish)
      .then((success) => {
        alert("Add new dish successfully!");
        render_menu();
        LOADING_SERVICES.turn_off_loading_screen();
      })
      .catch((error) => {
        console.log(error);
        LOADING_SERVICES.turn_off_loading_screen();
      });
  }
};
window.add_new_dish = add_new_dish;

// RENDER MENU
let render_menu = () => {
  LOADING_SERVICES.turn_on_loading_screen();
  MENU_SERVICES.get_data_from_API()
    .then((success) => {
      MENU = success.data;
      DISH_CONTROLLER.render_menu(MENU, "menu-table");
      LOADING_SERVICES.turn_off_loading_screen();
    })
    .catch((error) => {
      console.log(error);
      LOADING_SERVICES.turn_off_loading_screen();
    });
};
render_menu();

// DELETE DISH
let delete_dish = (id) => {
  LOADING_SERVICES.turn_on_loading_screen();
  MENU_SERVICES.delete_data(id)
    .then((success) => {
      render_menu();
      LOADING_SERVICES.turn_off_loading_screen();
    })
    .catch((error) => {
      console.log(error);
      LOADING_SERVICES.turn_off_loading_screen();
    });
};
window.delete_dish = delete_dish;

// PUT DATA BACK TO FORM
let put_data_back_to_form = (ID) => {
  LAYOUT_CONTROLLER.render_edit_form();
  let index = DISH_CONTROLLER.find_index(MENU, "id", ID);
  let dish = MENU[index];
  DISH_CONTROLLER.add_value_for_edit_input(dish);
};
window.put_data_back_to_form = put_data_back_to_form;

// UPDATE DISH INFO
let update_dish_info = () => {
  let id = DOM_MODELS.edit_el_array()[0].value;
  let type = DOM_MODELS.edit_el_array()[2].value;
  let dish = DISH_EDITED();
  let valid = VALIDATE_CONTROLLER.null_check(type, "type-edit-notice");
  if (valid) {
    LOADING_SERVICES.turn_on_loading_screen();
    MENU_SERVICES.update_data(id, dish)
      .then((success) => {
        render_menu();
        LOADING_SERVICES.turn_off_loading_screen();
      })
      .catch((error) => {
        console.log(error);
        LOADING_SERVICES.turn_off_loading_screen();
      });
  }
};
window.update_dish_info = update_dish_info;

// FIND DISH
let find_dish = () => {
  let ID = document.getElementById("dish-edited").value;
  let null_check = VALIDATE_CONTROLLER.null_check(ID, "id-edit-notice");
  if (null_check) {
    let idIndex = DISH_CONTROLLER.find_index(MENU, "id", ID * 1);
    let nameIndex = DISH_CONTROLLER.find_index(MENU, "name", ID);
    if (idIndex != -1 || nameIndex != -1) {
      let index = null;
      if (idIndex != -1) {
        index = idIndex;
      } else {
        index = nameIndex;
      }
      let dish = MENU[index];
      LAYOUT_CONTROLLER.render_edit_form();
      DISH_CONTROLLER.add_value_for_edit_input(dish);
    } else {
      document.getElementById(
        "id-edit-notice"
      ).innerHTML = `<span class="notice-span text-danger">
      "ID or name of this dish has not been in your menu yet. Please create it first."
      </span>`;
    }
  }
};
window.find_dish = find_dish;

// FORM RESET
let reset_add_form = () => {
  DISH_CONTROLLER.add_value_for_input(
    DOM_MODELS.add_el_array,
    "",
    "",
    "",
    "Available",
    ""
  );
  VALIDATE_CONTROLLER.null_check(1, "name-notice");
  VALIDATE_CONTROLLER.null_check(1, "type-notice");
};
window.reset_add_form = reset_add_form;

let reset_edit_form = () => {
  DISH_CONTROLLER.add_value_for_input(
    DOM_MODELS.edit_el_array(),
    DOM_MODELS.edit_el_array()[0].value,
    DOM_MODELS.edit_el_array()[1].value,
    "",
    "",
    "Available",
    ""
  );
  VALIDATE_CONTROLLER.null_check(1, "type-edit-notice");
};
window.reset_edit_form = reset_edit_form;
