function render_user_info() {
  let contentHTML = "";
  for (let i = 0; i < userArray.length; i++) {
    let user = userArray[i];
    let contentRendered = `<tr>
    <td>${user.id}</td>
    <td>${user.taiKhoan}</td>
    <td>${user.matKhau}</td>
    <td>${user.hoTen}</td>
    <td>${user.email}</td>
    <td>${user.ngonNgu}</td>
    <td>${user.loaiND}</td>
    <td>
    <button class="update-btn btn btn-warning" 
    data-toggle="modal" 
    data-target="#myModal" 
    onclick="change_user_info(${user.id})">
    <i class="fa fa-redo"></i>
    </button>
    <button class="delete-btn btn btn-danger" 
    onclick="delete_user(${user.id})">
    <i class="fa fa-trash-alt"></i>
    </button>
    </td>
    </tr>`;
    contentHTML += contentRendered;
  }
  document.getElementById("tblDanhSachNguoiDung").innerHTML = contentHTML;
}
