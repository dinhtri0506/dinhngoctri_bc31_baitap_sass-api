let render_teacher = (array) => {
  let contentHTML = "";
  let item = array.length;
  if (item > 8) {
    item = 8;
  }
  for (let i = 0; i < item; i++) {
    let teacher = array[i];
    let contentRendered = `<div class="item wow animation-delay-${i} animate__fadeInLeft">
<div class="teacher-img">
  <img src="./img/${teacher.hinhAnh}" alt="" />
</div>
<div class="teacher-info">
  <span>${teacher.ngonNgu}</span>
  <h3>${teacher.hoTen}</h3>
  <p>${teacher.moTa}</p>
</div>
</div>`;
    contentHTML += contentRendered;
  }
  document.getElementById("teacherTable").innerHTML = contentHTML;
};
