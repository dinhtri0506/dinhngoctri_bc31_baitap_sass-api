let add_account_validate = () => {
  let no_blank = validate_field_not_blank(
    domArray[0].value,
    noticeDomArray[0],
    "Tài khoản"
  );
  let condition = () => {
    let index = find_index(userArray, "taiKhoan", domArray[0].value);
    if (index == -1) {
      return true;
    }
    return false;
  };
  if (no_blank) {
    return validate_duplicate(
      condition(),
      noticeDomArray[0],
      "Tài khoản không được trùng nhau!"
    );
  }
  return false;
};

let update_account_validate = () => {
  let no_blank = validate_field_not_blank(
    domArray[0].value,
    noticeDomArray[0],
    "Tài khoản"
  );
  let condition = () => {
    let index = find_index(userArray, "taiKhoan", domArray[0].value);
    if (index != -1) {
      return true;
    }
    return false;
  };
  if (no_blank) {
    return validate_duplicate(
      condition(),
      noticeDomArray[0],
      "Tài khoản này không tồn tại! Cần tạo mới trước khi cập nhật!"
    );
  }
  return false;
};
