function validate() {
  //   let domArray = dom(
  //     "#TaiKhoan",
  //     "#HoTen",
  //     "#MatKhau",
  //     "#Email",
  //     "#loaiNguoiDung",
  //     "#loaiNgonNgu",
  //     "#MoTa",
  //     "#HinhAnh"
  //   );

  //   let noticeDomArray = dom(
  //     "#account-notice",
  //     "#name-notice",
  //     "#password-notice",
  //     "#email-notice",
  //     "#user-type-notice",
  //     "#language-notice",
  //     "#describe-notice",
  //     "#image-notice"
  //   );

  let callback_validate = (
    not_blank,
    domArray,
    noticeDomArray,
    fieldName,
    validate,
    ...rest
  ) => {
    let no_blank = not_blank(domArray, noticeDomArray, fieldName);
    if (no_blank) {
      return validate(...rest);
    }
    return false;
  };

  let can_be_extend_later = () => {
    return true;
  };

  let name = () => {
    let regEx = /^([a-zA-Z]+[\s]?)+$/;
    let name = removeAscent(domArray[1].value);
    let callback = callback_validate(
      validate_field_not_blank,
      domArray[1].value,
      noticeDomArray[1],
      "Họ tên",
      validate_format,
      regEx,
      name,
      noticeDomArray[1],
      "Họ tên không được chứa số hoặc ký tự đặc biệt."
    );
    return callback;
  };

  let password = () => {
    let regEx = /^(?=.*\d)(?=.*[A-Z])(?=.*\W)\S{6,8}$/;
    let callback = callback_validate(
      validate_field_not_blank,
      domArray[2].value,
      noticeDomArray[2],
      "Mật khẩu",
      validate_format,
      regEx,
      domArray[2].value,
      noticeDomArray[2],
      "Mật khẩu phải chứa ít nhất một ký tự hoa, một ký tự số, một ký tự đặc biệt và có độ dài từ 6 đến 8 ký tự."
    );
    return callback;
  };

  let email = () => {
    let regEx = /^[a-zA-Z]([\w\.\_\-]){3,11}@([a-zA-Z]{2,6})\.([a-zA-Z]{2,3})$/;
    let noticeContentHTML = `Email không hợp lệ <br />
    Email hợp lệ có dạng: tên@mail.com <br />
    <ul class="list-style-none text-secondary">
    <li><b>tên:</b> có từ 4 đến 12 ký tự (bao gồm chữ cái, số, dấu chấm, dấu gạch nối và dấu gạch chân) với ký tự đầu tiên là chữ cái</li>
    <li><b>mail:</b> có từ 2 đến 6 ký tự (chỉ bao gồm chữ cái)</li>
    <li><b>com:</b> có từ 2 đến 3 ký tự (chỉ bao gồm chữ cái)</li>
    <li><b>ví dụ: alice_19@gmail.com</b></li>
    </ul>`;
    let callback = callback_validate(
      validate_field_not_blank,
      domArray[3].value,
      noticeDomArray[3],
      "Email",
      validate_format,
      regEx,
      domArray[3].value,
      noticeDomArray[3],
      noticeContentHTML
    );
    return callback;
  };

  let user_type = () => {
    let callback = callback_validate(
      validate_field_not_blank,
      domArray[4].value,
      noticeDomArray[4],
      "Loại người dùng",
      can_be_extend_later
    );
    return callback;
  };

  let language = () => {
    let callback = callback_validate(
      validate_field_not_blank,
      domArray[5].value,
      noticeDomArray[5],
      "Ngôn ngữ",
      can_be_extend_later
    );
    return callback;
  };

  let describe = () => {
    let callback = callback_validate(
      validate_field_not_blank,
      domArray[6].value,
      noticeDomArray[6],
      "Mô tả",
      can_be_extend_later
    );
    return callback;
  };

  let image = () => {
    let callback = callback_validate(
      validate_field_not_blank,
      domArray[7].value,
      noticeDomArray[7],
      "Hình ảnh",
      can_be_extend_later
    );
    return callback;
  };

  let valid =
    name() &
    password() &
    email() &
    user_type() &
    language() &
    describe() &
    image();
  return valid;
}
