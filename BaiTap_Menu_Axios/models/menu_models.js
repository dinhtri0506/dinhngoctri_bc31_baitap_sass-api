import { DISH_CONTROLLER } from "../controller/dish_controller.js";

export let DOM_MODELS = {
  add_el_array: DISH_CONTROLLER.dom_all_dish_input(
    "#name-of-dish",
    "#type-of-dish",
    "#price-of-dish",
    "#dish-status",
    "#description-of-dish"
  ),
  edit_el_array: () => {
    let editInputElArray = DISH_CONTROLLER.dom_all_dish_input(
      "#id-edit-form",
      "#name-edit-form",
      "#type-edit-form",
      "#price-edit-form",
      "#status-edit-form",
      "#description-edit-form"
    );
    return editInputElArray;
  },
};
export let DISH = () => {
  let input = DOM_MODELS.add_el_array;
  let dish = {
    name: input[0].value,
    type: input[1].value,
    price: input[2].value * 1,
    status: input[3].value,
    description: input[4].value,
  };
  return dish;
};
export let DISH_EDITED = () => {
  let input = DOM_MODELS.edit_el_array();
  let dish = {
    id: input[0].value,
    name: input[1].value,
    type: input[2].value,
    price: input[3].value * 1,
    status: input[4].value,
    description: input[5].value,
  };
  return dish;
};
