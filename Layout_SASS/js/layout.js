var navbar = document.getElementById("header");

window.onscroll = function () {
  scrollFunction();
};

function scrollFunction() {
  if (
    document.body.scrollTop > 200 ||
    document.documentElement.scrollTop > 200
  ) {
    navbar.classList.add("navbar-position-fixed");
  } else {
    navbar.classList.remove("navbar-position-fixed");
  }
}
