import { DOM_MODELS } from "../models/menu_models.js";

export let VALIDATE_CONTROLLER = {
  null_check: (value, noticeElId) => {
    if (value == "") {
      document.getElementById(
        noticeElId
      ).innerHTML = `<span class="notice-span text-danger">This field can't be blanked!</span>`;
      return false;
    }
    document.getElementById(noticeElId).innerHTML = "";
    return true;
  },
  add_new_null_check: () => {
    let name_validate = VALIDATE_CONTROLLER.null_check(
      DOM_MODELS.add_el_array[0].value,
      "name-notice"
    );
    let type_validate = VALIDATE_CONTROLLER.null_check(
      DOM_MODELS.add_el_array[1].value,
      "type-notice"
    );
    let valid = name_validate & type_validate;
    return valid;
  },
};
