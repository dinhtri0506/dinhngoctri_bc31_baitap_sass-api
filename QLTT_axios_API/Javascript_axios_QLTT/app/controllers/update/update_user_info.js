function update_user_info() {
  // let domArray = dom(
  //   "#TaiKhoan",
  //   "#HoTen",
  //   "#MatKhau",
  //   "#Email",
  //   "#loaiNguoiDung",
  //   "#loaiNgonNgu",
  //   "#MoTa",
  //   "#HinhAnh"
  // );
  let taiKhoan = domArray[0].value;
  let index = find_index(userArray, "taiKhoan", taiKhoan);
  let id = index + 1;
  let user = create_user(domArray);
  loading_screen_on();
  axios({
    url: "https://62b07878e460b79df0469b79.mockapi.io/QLTT_user/" + id,
    method: "PUT",
    data: user,
  })
    .then(function (result) {
      get_data_from_api();
      loading_screen_off();
    })
    .catch(function (error) {
      console.log("error: ", error);
      loading_screen_off();
    });
}
