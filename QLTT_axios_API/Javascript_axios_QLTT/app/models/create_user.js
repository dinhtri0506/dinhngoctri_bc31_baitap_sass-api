function create_user(domArray) {
  let infoArray = [];
  for (let i = 0; i < domArray.length; i++) {
    let info = domArray[i].value;
    infoArray.push(info);
  }
  let user = new User(
    infoArray[0],
    infoArray[1],
    infoArray[2],
    infoArray[3],
    infoArray[4],
    infoArray[5],
    infoArray[6],
    infoArray[7]
  );
  return user;
}
